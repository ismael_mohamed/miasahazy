<?php

namespace App\Service;

use App\Entity\Affiliate;
use Swift_Mailer;
use Swift_Message;
use Symfony\Component\Templating\EngineInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class MailerService
{
    /** @var Swift_Mailer */
    private Swift_Mailer $mailer;

    private Environment $templateEngine;

    /**
     * @param Swift_Mailer $mailer
     * @param Environment $templateEngine
     */
    public function __construct(Swift_Mailer $mailer, Environment $templateEngine)

    {
        $this->mailer = $mailer;
        $this->templateEngine = $templateEngine;
    }

    /**
     * @param Affiliate $affiliate
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function sendActivationEmail(Affiliate $affiliate): void
    {
        $message = (new Swift_Message())
            ->setSubject('Account activation')
            ->addFrom('contact@hodyandco.com')
            ->setTo($affiliate->getEmail())
            ->setBody(
                $this->templateEngine->render(
                    'emails/affiliate_activation.html.twig',
                    [
                        'token' => $affiliate->getToken(),
                    ]
                ),
                'text/html'
            );

        //dd($this->mailer->send($message));

        $this->mailer->send($message);
    }
}
