<?php


namespace App\DataFixtures;

use App\Entity\Job;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class JobFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @param ObjectManager $manager
     *
     * @return void
     */
    public function load(ObjectManager $manager) : void
    {
        $faker = Factory::create();

        $category = ['category-design', 'category-programming', 'category-manager', 'category-administrator'];
        $type = ['part-time', 'full-time'];
        $public = [true, false];
        $status = [true, false];

        for ($i = 1; $i <= 130; $i++) {
            $job = new Job();
            $job->setCategory($manager->merge($this->getReference($faker->randomElement($category))));
            $job->setType($faker->randomElement($type));
            $job->setCompany($faker->company);
            $job->setPosition($faker->jobTitle);
            $job->setLocation($faker->city);
            $job->setDescription(implode('<br/><br/>', $faker->paragraphs(6)));
            $job->setHowToApply('Send your resume to ' . $faker->companyEmail);
            $job->setPublic($faker->randomElement($public));
            $job->setActivated($faker->randomElement($status));
            $job->setToken($job->getPosition().'-'.$i);
            $job->setEmail($faker->email);

            $manager->persist($job);
        }

        $manager->flush();
    }

    /**
     * @return array
     */
    public function getDependencies(): array
    {
        return [
            CategoryFixtures::class,
        ];
    }
}
