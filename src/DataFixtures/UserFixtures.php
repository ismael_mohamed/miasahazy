<?php


namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private UserPasswordEncoderInterface $encoderPassword;

    public function __construct(UserPasswordEncoderInterface $encoderPassword)
    {
        $this->encoderPassword = $encoderPassword;
    }

    /**
     * @param ObjectManager $manager
     *
     * @return void
     */
    public function load(ObjectManager $manager): void
    {
        $user = new User();
        $user->setEmail('user@email.com');
        $user->setPassword($this->encoderPassword->encodePassword($user, 'user'));
        $user->setRoles(['ROLE_USER']);
        $user->setEnabled(true);

        $admin = new User();
        $admin->setEmail('admin@email.com');
        $admin->setPassword($this->encoderPassword->encodePassword($user, 'admin'));
        $admin->setRoles(['ROLE_ADMIN', 'ROLE_USER']);
        $admin->setEnabled(true);

        $manager->persist($user);
        $manager->persist($admin);

        $manager->flush();
    }
}
