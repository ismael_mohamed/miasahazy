<?php

namespace App\Controller\Admin;

use App\Entity\Agency;
use App\Entity\Category;
use App\Entity\Department;
use App\Entity\Event;
use App\Entity\Job;
use App\Entity\Newsletter;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return parent::index();
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Miasahazy');
    }

    public function configureMenuItems(): iterable
    {
        return [
            MenuItem::linktoDashboard('Dashboard', 'fa fa-home'),

            MenuItem::section('General'),
            MenuItem::linkToCrud('Jobs', 'fa fa-building', Job::class),
            MenuItem::linkToCrud('Category', 'fa fa-building', Category::class)
        ];

    }
}
