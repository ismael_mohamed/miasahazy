<?php

namespace App\Controller\Admin;

use App\Entity\Job;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\UrlField;

class JobCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Job::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Job')
            ->setEntityLabelInPlural('Jobs')
            ->setPageTitle('index', '%entity_label_plural% listing');
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                return $action->setIcon('fa fa-plus')->setLabel('Add Job');
            });
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('type', 'Type'),
            TextField::new('company', 'Company'),
            ImageField::new('logo', 'Logo')->hideOnIndex()->setUploadDir('public/uploads/jobs'),
            TextEditorField::new('description', 'Description')->hideOnIndex(),
            TextField::new('howToApply', 'How to Apply?')->hideOnIndex(),
            BooleanField::new('public', 'Public?'),
            BooleanField::new('activated', 'Activated?'),
            AssociationField::new('category', 'Category')->hideOnIndex(),
            TextField::new('email', 'email')->hideOnIndex(),
            UrlField::new('url', 'URL'),
            TextField::new('position', 'Position'),
            TextField::new('location', 'Location'),
            DateTimeField::new('createdAt', 'Created at')->onlyOnIndex(),
            TextField::new('token', 'Token'),
        ];
    }
}
