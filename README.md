# Miasahazy

Ce projet est une installation docker compose d'un projet Symfony ([Jobeet Symfony 4](https://jobeet-tutorial.readthedocs.io/en/latest/)) pour un site utilisant [Caddy Server](https://caddyserver.com/) comme serveur web, PostgreSQL comme base de données et Adminer pour la gestion de la base de données.\

La structure du projet est basé sur le [Symfony Docker Starter](https://github.com/dunglas/symfony-docker) de [Kévin Gunglas](https://github.com/dunglas).

**Qu'est ce que Miasahazy ?**

Miasahazy est un site d'offres d'emploi entièrement conçu sous Symfony 5.\
De base, il a été écrit par l'équipe Symfony pour la version Symfony 1.x et je l'ai légerment adapter pour Symfony 5.

Miasahazy est composé de deux mots, Miasa et Hazy.\
Miasa est un mot malgache, cela veut dire `Travailler/Travail` \
Hazy quant a lui est un mot mahorais et veut dire la même chose, `Travailler/Travail` 

**NOTE** : Désole pour les paréceux, je n'ai pas encore mis un Makefile dans le projet pour faciliter les différents commandes qui pourrait être utilisées dans le projet.

### Prérequis
- Docker ([Documentation](https://docs.docker.com/engine/install/))
- Docker Compose ([Documentation](https://docs.docker.com/compose/install/))

**NOTE** : Dans la dernière version de Docker, Docker Compose a été intégré à Docker. Donc, plus besoin de Docker compose.

### Table des matières
- [La structure du projet](#la-structure) - La structure du projet
- [Lancer le projet](#running) - Lancer le projet en local
- [Lonfiguration et logs](#opt-config) - Les configurations (`docker-compose.yml, Dockerfile`) et logs des containers

### <a name="la-structure"></a>La structure du projet
Comme je le mentionne un peu plus haut, le projet tourne sur le serveur web Caddy.\
Tu te pose surement la question, de, pourquoi Caddy Server ?\
Et bien, simplement parce que Caddy possède de base un HTTPS contrairement a NGINX ou Apache.

Voici plus de détails concernant la structure du projet :

- Environnement de production, dévelopement
- HTTPS automatique avec Caddy (en dev et en prod)
- Prise en charge de HTTP/2, HTTP/3 et Preload
- Mercure Hub intégré
- Support de Vulcain
- Seulement 2 services principaux (PHP-FPM et Caddy Server)

```
.
├── assets
├── bin
├── config
├── docker
├── docs
├── migrations
├── node_modules
├── public
├── src
├── templates
├── tests
├── translations
├── var
├── vendor
├── Dockerfile
├── README.md
├── composer.json
├── composer.lock
├── docker-compose.override.yml
├── docker-compose.prod.yml
├── docker-compose.yml
├── package.json
├── phpunit.xml.dist
├── symfony.lock
├── webpack.config.js
└── yarn.lock
```

###<a name="running"></a>Lancer le projet

Pour t'épargner la mémorisation des différentes commandes, je te conseille de mettre en place un [Makefile](https://makefiletutorial.com/), ça aide grave !!

Pour commencer, place-toi, à l'aide d'un terminal, à la racine du projet.
```bash
$ cd miasahazy
```

Il faut, ensuite, lancer le projet. Pour cela tape juste la commande ci-dessous dans ton terminal.
``` bash
$ docker-compose -f docker-compose.yml up -d --build
```

**INFO** : Ajoute le `--build` uniqument si c'est la première fois que tu lance le projet ou si tu as fait des changement dans le fichier `Dockerfile`.\
L'argument `--build` t'évite de taper deux commande à asuite, comme ceci : `docker-compose build` puis `docker-compose -f docker-compose.yml up -d`.

Pour afficher la progression du lancement des containres sont prêt, tu peux lancer la commande suivante :
```bash
$ docker-compose logs -f #Afficher les logs des containers
```

#### <a name="connect"></a>Installer le projet
Une fois que les containers sont lancés, il faut que tu installes le projet.\
Suit les étapes suivantes pour intaller le projet.

Depuis la racine du projet :
- Installer les dépendances js et css : `$ yarn install` puis `$ yarn encore dev`
- Faire les migrations dans des entités : `$ docker-compose exec php php bin/console doctrine:schema:update --force`
- Jouer les fixtures pour les fausses données : `$ docker-compose exec php php bin/console doctrine:fixtures:load -n`

Voilà, il est maintenant temps d'ouvrir un navigateur est accéder au site [https://localhost](https://localhost)

**NOTE** : Il est possible que le HTTPS ne fonctionne pas.\
Exécute l'une des commandes suivantes selon ton système d'exploitation :
```bash
# macOS
$ docker cp $(docker-compose ps -q caddy):/data/caddy/pki/authorities/local/root.crt /tmp/root.crt && sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain /tmp/root.crt

# Linux
$ docker cp $(docker-compose ps -q caddy):/data/caddy/pki/authorities/local/root.crt /usr/local/share/ca-certificates/root.crt && sudo update-ca-certificates

```

#### <a name="connect"></a>Accès Admin
Le projet contient également un espace d'administration.\
Tu peux y accéder avec cette URL : [https://localhost/admin/jobs](https://localhost/admin/jobs)

Un fake user est disponible pour se connecter entant que user du front et du back :\
User : `admin@email.com`\
Password : `admin`


### <a name="setup"></a>Le configuration


Un fichier `.env` a été inclus pour définir plus facilement les variables d'environnement et ainsi nous éviter à modifier directement le fichier `docker-compose.yml` lui-même.

Si jamais tu veux modifier les valeurs du fichier .env pour faire des tests, duplique le fichier et renomme-le, comme : `.env.test` ou `.env.local`

Le fichier `.env` se trouve à la racine du projet (`PWD == ./miasahazy`).

```dotenv
POSTGRES_ROOT_PASSWORD=ZfqQkDsz7!f$
POSTGRES_DB=misahazy
POSTGRES_USER=user
POSTGRES_PASSWORD=password
POSTGRES_HOST=database
POSTGRES_VERSION=13
```

Les principales variables d'environnement.
- `POSTGRES_ROOT_PASSWORD `: Mot de passe root de la base de données. Par défaut 'root' est le user.
- `POSTGRES_DB `: Nom de la base de données utilisé.
- `POSTGRES_USER `: Nom de l'utilisateur.
- `POSTGRES_PASSWORD`: Mot de passe de la base de données.
- `POSTGRES_HOST`: Le host pour se connecter à la base de données (`database`).
- `POSTGRES_VERSION`: La version de la base de données (`Version 13`).

```yaml
#docker-compose.yml
...
database:
    image: postgres:${POSTGRES_VERSION:-13}-alpine
    environment:
        POSTGRES_DB: ${POSTGRES_DB:-hodyandco}
        POSTGRES_PASSWORD: ${POSTGRES_PASSWORD:-password}
        POSTGRES_USER: ${POSTGRES_USER:-user}
    volumes:
        - database-data:/var/lib/postgresql/data:rw
    ports:
        - 5432
...
```

### Logs des containers
Tu peux avoir besoin d'afficher les logs des différents containers. Pour cela, utilise les commandes suivantes :

**Pour afficher les logs depuis la création des tous les containers :**
```bash
$ docker-compose logs -f
$ docker-compose logs -f caddy #Uniquement les logs du container caddy
```
**Pour afficher les logs créés à l'instant**
```bash
$ docker-compose logs -f --tail=0
$ docker-compose logs -f --tail=0 caddy #Uniquement les logs du container caddy
```
Le `-f` permet d'afficher les logs en temps réel.

Et bien, voilà, tu peux t'amuser sur le projet avec toutes ces informations.
